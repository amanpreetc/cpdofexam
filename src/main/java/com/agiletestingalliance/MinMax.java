package com.agiletestingalliance;

public class MinMax {

    public int function(int var1, int var2) {
        if ( var2 > var1) {
            return var2;
    }
        else {
            return var1;
    }

}
